FROM openjdk:11

EXPOSE 8080
ADD ./target/quarkus-app/ .
ENTRYPOINT ["java", "-jar", "quarkus-run.jar"]